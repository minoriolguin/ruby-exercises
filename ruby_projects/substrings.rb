def substrings(phrase, dictionary)
	phrase_array = phrase.downcase.split(" ")
	hash = Hash.new(0)

	phrase_array.each do |word|
		dictionary.each do |dictionary_word|
			hash[dictionary_word] += 1 if word.include?(dictionary_word)
		end
	end
	hash
end

dictionary = ["below","down","go","going","horn","how","howdy","it","i","low","own","part","partner","sit"]
puts substrings("below", dictionary)
puts substrings("Howdy partner, sit down! How's it going?", dictionary)

