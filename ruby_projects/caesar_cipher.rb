def caesar_cipher(string, shift_number)
  letter_array = string.downcase!.split("")
  new_string = Array.new(letter_array.length)
  letter_array.each do |letter|
    letter = letter.ord
    if letter + shift_number <= 122 && letter >= 97
      letter = letter + shift_number
    elsif letter >= 32 && letter < 97
      letter = letter
    else 
      difference = letter + shift_number - 122
      letter = 96 + difference
    end
    letter = letter.chr
    new_string.push(letter)
  end
  new_string = new_string.join("").capitalize!
  puts new_string
end

string = "This is a sentence."
caesar_cipher(string, 12)

string = "What a string!"
caesar_cipher(string, 5)

